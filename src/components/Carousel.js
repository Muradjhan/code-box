import React, {Component} from 'react';
import ReactCardCarousel from 'react-card-carousel';

class MyCarousel extends Component {
    static get CONTAINER_STYLE() {
        return {
            backgroundColor: '#fff',
            position: "relative",
            // height: "100vh",
            width: "100%",
            boxSizing: 'border-box',
            // display: "flex",
            // flex: 1,
            // justifyContent: "center",
            // alignItems: "middle"
        };
    }

    static get CARD_STYLE() {
        return {
            backgroundColor: '#fff',
            height: "340px",
            width: "700px",
            paddingTop: "80px",
            textAlign: "center",
            // background: "#52C0F5",
            color: "black",
            fontFamily: "sans-serif",
            fontSize: "12px",
            // textTransform: "uppercase",
            borderRadius: "10px",
            boxSizing: "border-box"
        };
    }

    render() {
        return (
            <div className="myCarousel" style={MyCarousel.CONTAINER_STYLE}>
                <ReactCardCarousel className="row" autoplay={false} autoplay_speed={2500}>
                    <div className="col-lg-12 w-100" style={MyCarousel.CARD_STYLE}>
                        <div className="card w-100 h-100 border-0 bg-transparent text-center">
                            <div className="card-header border-0 bg-transparent">
                                <img style={{transform: 'translateY(-170px)',}} src="images/mainPerson.png"
                                     alt="Muhiddin Jumayev"/>
                            </div>
                            <div style={{
                                fontSize: '20px',
                                lineHeight: '24px',
                                textAlign: 'center',
                                color: '#000000',
                                transform: 'translateY(-170px)'
                            }}
                                 className="card-body border-0 bg-transparent font-weight-bold">Muhiddin Jumayev <br/>
                                <span style={{color: '#1DD1A1'}}>Coder</span></div>
                            <div className="card-footer border-0 bg-transparent">
                                <p style={{
                                    transform: 'translateY(-130px)',
                                    fontSize: '16px',
                                    lineHeight: '19px',
                                    textAlign: 'center',
                                    color: '#343434'
                                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Volutpat commodo sed egestas egestas.
                                    Feugiat in ante metus dictum at tempor commodo ullamcorper a. Vitae auctor eu augue
                                    ut lectus. Curabitur vitae nunc sed velit dignissim sodales ut eu sem. Quam
                                    nulla porttitor massa id neque aliquam vestibulum.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 w-100" style={MyCarousel.CARD_STYLE}>
                        <div className="card w-100 h-100 border-0 bg-transparent text-center">
                            <div className="card-header border-0 bg-transparent">
                                <img style={{transform: 'translateY(-170px)',}} src="images/mainPerson.png"
                                     alt="Muhiddin Jumayev"/>
                            </div>
                            <div style={{
                                fontSize: '20px',
                                lineHeight: '24px',
                                textAlign: 'center',
                                color: '#000000',
                                transform: 'translateY(-170px)'
                            }}
                                 className="card-body border-0 bg-transparent font-weight-bold">Muhiddin Jumayev <br/>
                                <span style={{color: '#1DD1A1'}}>Coder</span></div>
                            <div className="card-footer border-0 bg-transparent">
                                <p style={{
                                    transform: 'translateY(-130px)',
                                    fontSize: '16px',
                                    lineHeight: '19px',
                                    textAlign: 'center',
                                    color: '#343434'
                                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Volutpat commodo sed egestas egestas.
                                    Feugiat in ante metus dictum at tempor commodo ullamcorper a. Vitae auctor eu augue
                                    ut lectus. Curabitur vitae nunc sed velit dignissim sodales ut eu sem. Quam
                                    nulla porttitor massa id neque aliquam vestibulum.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 w-100" style={MyCarousel.CARD_STYLE}>
                        <div className="card w-100 h-100 border-0 bg-transparent text-center">
                            <div className="card-header border-0 bg-transparent">
                                <img style={{transform: 'translateY(-170px)',}} src="images/mainPerson.png"
                                     alt="Muhiddin Jumayev"/>
                            </div>
                            <div style={{
                                fontSize: '20px',
                                lineHeight: '24px',
                                textAlign: 'center',
                                color: '#000000',
                                transform: 'translateY(-170px)'
                            }}
                                 className="card-body border-0 bg-transparent font-weight-bold">Muhiddin Jumayev <br/>
                                <span style={{color: '#1DD1A1'}}>Coder</span></div>
                            <div className="card-footer border-0 bg-transparent">
                                <p style={{
                                    transform: 'translateY(-130px)',
                                    fontSize: '16px',
                                    lineHeight: '19px',
                                    textAlign: 'center',
                                    color: '#343434'
                                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Volutpat commodo sed egestas egestas.
                                    Feugiat in ante metus dictum at tempor commodo ullamcorper a. Vitae auctor eu augue
                                    ut lectus. Curabitur vitae nunc sed velit dignissim sodales ut eu sem. Quam
                                    nulla porttitor massa id neque aliquam vestibulum.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 w-100" style={MyCarousel.CARD_STYLE}>
                        <div className="card w-100 h-100 border-0 bg-transparent text-center">
                            <div className="card-header border-0 bg-transparent">
                                <img style={{transform: 'translateY(-170px)',}} src="images/mainPerson.png"
                                     alt="Muhiddin Jumayev"/>
                            </div>
                            <div style={{
                                fontSize: '20px',
                                lineHeight: '24px',
                                textAlign: 'center',
                                color: '#000000',
                                transform: 'translateY(-170px)'
                            }}
                                 className="card-body border-0 bg-transparent font-weight-bold">Muhiddin Jumayev <br/>
                                <span style={{color: '#1DD1A1'}}>Coder</span></div>
                            <div className="card-footer border-0 bg-transparent">
                                <p style={{
                                    transform: 'translateY(-130px)',
                                    fontSize: '16px',
                                    lineHeight: '19px',
                                    textAlign: 'center',
                                    color: '#343434'
                                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Volutpat commodo sed egestas egestas.
                                    Feugiat in ante metus dictum at tempor commodo ullamcorper a. Vitae auctor eu augue
                                    ut lectus. Curabitur vitae nunc sed velit dignissim sodales ut eu sem. Quam
                                    nulla porttitor massa id neque aliquam vestibulum.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 w-100" style={MyCarousel.CARD_STYLE}>
                        <div className="card w-100 h-100 border-0 bg-transparent text-center">
                            <div className="card-header border-0 bg-transparent">
                                <img style={{transform: 'translateY(-170px)',}} src="images/mainPerson.png"
                                     alt="Muhiddin Jumayev"/>
                            </div>
                            <div style={{
                                fontSize: '20px',
                                lineHeight: '24px',
                                textAlign: 'center',
                                color: '#000000',
                                transform: 'translateY(-170px)'
                            }}
                                 className="card-body border-0 bg-transparent font-weight-bold">Muhiddin Jumayev <br/>
                                <span style={{color: '#1DD1A1'}}>Coder</span></div>
                            <div className="card-footer border-0 bg-transparent">
                                <p style={{
                                    transform: 'translateY(-130px)',
                                    fontSize: '16px',
                                    lineHeight: '19px',
                                    textAlign: 'center',
                                    color: '#343434'
                                }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Volutpat commodo sed egestas egestas.
                                    Feugiat in ante metus dictum at tempor commodo ullamcorper a. Vitae auctor eu augue
                                    ut lectus. Curabitur vitae nunc sed velit dignissim sodales ut eu sem. Quam
                                    nulla porttitor massa id neque aliquam vestibulum.</p>
                            </div>
                        </div>
                    </div>
                </ReactCardCarousel>
            </div>
        );
    }
}

export default MyCarousel;

// class MyCarousel extends Component {
//
//     static get CARD_STYLE() {
//         return {
//             height: '200px',
//             width: '200px',
//             paddingTop: '80px',
//             textAlign: 'center',
//             background: '#52C0F5',
//             color: '#FFF',
//             fontSize: '12px',
//             textTransform: 'uppercase',
//             borderRadius: '10px',
//         };
//     }
//
//     render() {
//         return (
//             <ReactCardCarousel className="myCarousel" autoplay={ false } autoplay_speed={ 2500 }>
//                 <div style={ MyCarousel.CARD_STYLE }>
//                     First Card
//                 </div>
//                 <div style={ MyCarousel.CARD_STYLE }>
//                     Second Card
//                 </div>
//                 <div style={ MyCarousel.CARD_STYLE }>
//                     Third Card
//                 </div>
//                 <div style={ MyCarousel.CARD_STYLE }>
//                     Fourth Card
//                 </div>
//                 <div style={ MyCarousel.CARD_STYLE }>
//                     Fifth Card
//                 </div>
//             </ReactCardCarousel>
//         );
//     }
// }
