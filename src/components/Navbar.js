import React, {Component} from 'react';
import {Link} from "react-scroll";

class Navbar extends Component {
    render() {

        let boyImage = '/images/boy1.png'

        return (
            <React.Fragment>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div className="container">
                        <a className="navbar-brand" href="#">
                            <img src="images/logo.svg" alt="logo"/>
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarTogglerDemo01"
                                aria-controls="navbarTogglerDemo01" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                                <li className="nav-item active">
                                    <Link
                                        activeClass="active"
                                        to="about"
                                        spy={true}
                                        smooth={true}
                                        offset={-100}
                                        duration={500}
                                        className="nav-link">Loyiha haqida</Link>
                                </li>
                                <li className="nav-item">
                                    <Link
                                        activeClass="active"
                                        to="progLanguages"
                                        spy={true}
                                        smooth={true}
                                        offset={-100}
                                        duration={500}
                                        className="nav-link">Dasturlash tillari</Link>
                                </li>
                                <li className="nav-item">
                                    <Link
                                        activeClass="active"
                                        to="prices"
                                        spy={true}
                                        smooth={true}
                                        offset={-100}
                                        duration={500}
                                        className="nav-link">Narxlar</Link>
                                </li>
                            </ul>
                            <div className="navbar-right my-2 my-lg-0">
                                <div className="d-flex">
                                    <div className="dropdown">
                                        <button
                                            className="btn btn-secondary border-0 bg-transparent text-dark mr-5 lang-menu"
                                            type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            UZ <img src="images/icons/chevron-down.svg" alt=""/>
                                        </button>
                                        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a className="dropdown-item" href="#">RU</a>
                                            <a className="dropdown-item" href="#">ENG</a>
                                        </div>
                                    </div>
                                    <button className="btn btn-success text-white tugma">Ro’yxatdan o’tish</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </React.Fragment>
        );
    }
}

export default Navbar;