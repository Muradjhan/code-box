function Section5() {
    return (
        <section className="section-5" id="prices">
            <div className="container">
                <div className="row">
                    <div className="col-lg-7 col-12 sarlavha">
                        O’z rejangizni <br/>
                        tanlang
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-4">
                        <div className="card border-0">
                            <div className="card-header border-0 bg-transparent text-center">Simple</div>
                            <div className="card-body border-0 bg-transparent text-center">
                                <h2>99.000 UZS</h2>
                                <h5>Har 1 oyda to’lov</h5>
                                <p>Bu ta’rif to’plamini aktivlashtirish orqali
                                    siz tizimdan 1 oy muddatda to’liq
                                    foydalanishingiz mumkin.  Qayta
                                    aktivlashtirish har 1 oy muddatda
                                    amalga oshiriladi.</p>
                            </div>
                            <div className="card-footer border-0 bg-transparent text-center">
                                <button className="btn btn-outline-success">Obuna bo’lish</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card border-0">
                            <img src="images/icons/recommStick.svg" alt=""/>
                            <div className="card-header border-0 bg-transparent text-center">Middle</div>
                            <div className="card-body border-0 bg-transparent text-center">
                                <h2>269.000 UZS</h2>
                                <h5>Har 3 oyda to’lov</h5>
                                <p>Bu ta’rif to’plamini aktivlashtirish orqali
                                    siz tizimdan 3 oy muddatda to’liq
                                    foydalanishingiz mumkin.  Qayta
                                    aktivlashtirish har 3 oy muddatda
                                    amalga oshiriladi.</p>
                            </div>
                            <div className="card-footer border-0 bg-transparent text-center">
                                <button className="btn btn-outline-success">Obuna bo’lish</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card border-0">
                            <div className="card-header border-0 bg-transparent text-center">Edvanced</div>
                            <div className="card-body border-0 bg-transparent text-center">
                                <h2>499.000 UZS</h2>
                                <h5>Har 6 oyda to’lov</h5>
                                <p>Bu ta’rif to’plamini aktivlashtirish orqali
                                    siz tizimdan 6 oy muddatda to’liq
                                    foydalanishingiz mumkin.  Qayta
                                    aktivlashtirish har 6 oy muddatda
                                    amalga oshiriladi.</p>
                            </div>
                            <div className="card-footer border-0 bg-transparent text-center">
                                <button className="btn btn-outline-success">Obuna bo’lish</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Section5;