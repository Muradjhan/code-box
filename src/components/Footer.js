import React, {Component} from 'react';
import {Link} from "react-scroll";

class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-12">
                            <img src="images/icons/footer-logo.svg" alt="code-box-logo"/>
                        </div>
                        <div className="col-lg-5 ">
                            <ul>
                                <li>
                                    <Link
                                        activeClass="active"
                                        to="about"
                                        spy={true}
                                        smooth={true}
                                        offset={-100}
                                        duration={500}>Loyiha haqida</Link>
                                </li>
                                <li>
                                    <Link
                                        activeClass="active"
                                        to="progLanguages"
                                        spy={true}
                                        smooth={true}
                                        offset={-50}
                                        duration={500}>Dasturlash tillari</Link>
                                </li>
                                <li>
                                    <Link
                                        activeClass="active"
                                        to="prices"
                                        spy={true}
                                        smooth={true}
                                        offset={-100}
                                        duration={500}>Narxlar</Link>
                                </li>
                                <li>
                                    <Link
                                        activeClass="active"
                                        to="aboutUs"
                                        spy={true}
                                        smooth={true}
                                        offset={-50}
                                        duration={500}>Biz haqimizda</Link>
                                </li>
                            </ul>
                        </div>
                        <div className="col-lg-4 col-12">
                            <img src="images/icons/facebook.svg" alt="facebook logo"/>
                            <img src="images/icons/instagram.svg" alt="instagram logo"/>
                            <img src="images/icons/telegram.svg" alt="telegram logo"/>
                            <img src="images/icons/youtube.svg" alt="you-tube logo"/>
                        </div>
                    </div>
                    <p>© “Personal Development Process” MCHJ</p>
                </div>
            </footer>

        );
    }
}

export default Footer;