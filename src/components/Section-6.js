import MyCarousel from "./Carousel";


function Section6() {
    return (
        <section className="section-6" id="aboutUs">
            <div className="container">
                <div className="row row1">
                    <div className="col-lg-5 col-12 sarlavha">
                        Biz haqimizda
                        boshqalar fikri
                    </div>
                </div>
                <div className="row">
                    <MyCarousel/>
                </div>
            </div>
        </section>
    )
}

export default Section6;