// import './index.scss';

function Section1() {
    return (
        <section className="section-1" id="about">
            <div className="container">
                <div className="row">
                    <div className="col-lg-7 col-12">
                        <p className="sarlavha">
                            Agar bajarishdan qo’rqsangiz
                            buni albatta bajarib ko’ring</p>
                        <form className="sign-in-form">
                            <div className="input-group myInput">
                                <img className="greenBorder" src="images/icons/greenBorder.svg" alt=""/>
                                <label className="inputText w-100">Telefon nomer</label><br/>
                                <input className="border-0 " height='68px' width='400px' type="number"
                                       placeholder="+998934614188" max="15"/>
                            </div>
                            <div className="input-group myInput">
                                <img className="greenBorder" src="images/icons/greenBorder.svg" alt=""/>
                                <label className="inputText w-100">Parol</label><br/>
                                <input className="border-0" type="password" placeholder="******************"/>
                            </div>
                        </form>
                        <br/>
                        <button className="btn btn-success">Kirish</button>

                        <div className="toggle">
                            <input type="checkbox" className="check"/>
                            <b className="b switch"></b>
                            <b className="b track"></b>
                        </div>

                        <span>
                            Meni eslab qol
                        </span>
                    </div>
                    <div className="col-lg-5 col-12">
                        <a>
                            <img className="w-100 video" src="images/people3.svg" alt="" data-toggle="modal"
                                 data-target="#modal1"/>
                        </a>
                    </div>
                    <div style={{zIndex: "100000000"}} className="modal fade" id="modal1" tabIndex="-1" role="dialog"
                         aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div className="modal-content">
                                <img src="images/icons/close.svg" alt="" data-dismiss={'modal'}
                                     style={{position: 'absolute', top: '-40px', right: '0', cursor: 'pointer',}}/>
                                <div className="modal-body mb-0 p-0">
                                    <div className="embed-responsive embed-responsive-16by9 z-depth-1-half">
                                        <iframe width="460" height="230"
                                                src="//www.youtube.com/embed/IlVpkBf3McU?autoplay=1&amp;mute=0"
                                                frameBorder="0" allowFullScreen="" tabIndex="-1"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Section1;

function modalVideo() {
    return (
        <modal>

        </modal>
    )
}