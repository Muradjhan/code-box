function Section3() {
    return (
        <section className="section-3" id="progLanguages">
            <div className="container">
                <div className="row row-1">
                    <div className="col-lg-7 col1">
                        O’zingiz uchun dunyoda mashxur
                        dasturlash tillaridan birini
                        tanlang
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>PYTHON</h3>
                                <span className="font-weight-bold"> 128 ta </span>topshiriq | <span
                                className="font-weight-bold">38 ta</span> bo'lim
                            </div>
                            <div className="card-body">
                                <p>Python - vysokourovnevyy yazyk programirova
                                    niya obshchego naznacheniya, orientirovannyy na povyshenie proizvoditelnosti
                                    razrabotchika i
                                    chitaemosti koda. Syntaxxd Python minimalis tichen. V toni vremya standartnaya
                                    biblioteka
                                    vklyuchaet bolshoy ob'yom poleznyx fonksiyont</p>
                                <button className="btn btn-success text-white">Kursni bo'shlash</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>GO</h3>
                                <span className="font-weight-bold"> 128 ta </span>topshiriq | <span
                                className="font-weight-bold">38 ta</span> bo'lim
                            </div>
                            <div className="card-body">
                                <p>Java - silno tipirovannyy obyektno orientirovann  yysag yazyk programmirovaniya,
                                    razrabotannyy
                                    kompaniey Sun Microsystems. V nastoyashchee  vremya proekt matlenejit OpenSource i
                                    rasprof ranya
                                    yaetsya po litsenzii GPL. V OpenJDK vnosyat vklad krupnye kompanii, sochi kak
                                    </p>
                                <button className="btn btn-success text-white">Kursni bo'shlash</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card">
                            <div className="card-header">
                                <h3>Java</h3>
                                <span className="font-weight-bold"> 128 ta </span>topshiriq | <span
                                className="font-weight-bold">38 ta</span> bo'lim
                            </div>
                            <div className="card-body">
                                <p>Go - kompilyumemyy multilogo yazyk program
                                    movaniya, razrabotannyy vnutri kompanii Google.
                                    Razrabotka Go nachalas v sentabr 2007 goda, ego neposredstvennym projektirovaniem zanima
                                    lis
                                    Robert Grizmer, Rob Pikk i Ken Thompson, zanimavshiesya do etogo proektom
                                    razrabotki </p>
                                <button className="btn btn-success text-white">Kursni bo'shlash</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>)
}

export default Section3;