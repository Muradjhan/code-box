function Section2() {
    return (
        <section className="learn-prog container">

            <h1 className="learn-prog-header">
                <b>
                    Dasturlashni amaliy
                    <div>
                        tarzda CodeBox bilan o'rganing.
                    </div>
                </b>
            </h1>

            <div className="learn-prog-row row ">

                <div className="learn-prog-col col-lg-4 col-md-10 order-lg-1 order-md-2 mx-md-auto">
                    <img className="learn-prog-col-img w-100" src="images/Group 40.png"/>

                </div>

                <div className="learn-prog-col col-lg-5 ml-lg-auto order-lg-2 order-md-1 col-md-8 mt-5">
                    <h2 className="learn-prog-col-text-header">
                        01. Dasturlash tilini tanlang.
                    </h2>
                    <p className="learn-prog-col-text">
                        Hozirgi kunda eng ommabop dasturlash tillarini CodeBoxga kiritdik va buni doimiy tarzda yangilab
                        boramiz.
                    </p>
                </div>

            </div>

            <div className="learn-prog-row row ">


                <div className="learn-prog-col col-lg-4 col-md-10 order-lg-2 order-md-2 mx-md-auto">
                    <h2 className="learn-prog-col-text-header">
                        02. Topshiriqlarni bajarish orqali o'rganing.
                    </h2>
                    <p className="learn-prog-col-text">
                        Dasturlash algoritmlari, suhbat uchun topshiriqlari va murakkab topshiriqlarni bajaring.
                    </p>
                </div>


                <div className="learn-prog-col col-lg-4 col-md-10 order-lg-2 order-md-2 mx-md-auto">
                    <img className="learn-prog-col-img w-100" src="images/Group 71.svg"/>

                </div>

            </div>

            <div className="learn-prog-row row mt-5">

                <div className="learn-prog-col col-lg-4 col-md-10 order-lg-1 order-md-2 mx-md-auto">
                    <img className="learn-prog-col-img w-100" src="images/s2p3.png"/>

                </div>

                <div className="learn-prog-col col-lg-5 ml-lg-auto order-lg-2 order-md-1 col-md-8">
                    <h2 className="learn-prog-col-text-header">
                        03. Yakunlangan dasturlash tiliga tegishli oid fraymvork o'rganing.
                    </h2>
                    <p className="learn-prog-col-text">
                        Web, Mobile, Desktop va boshqa yo'nalishlarda
                        dasturlash tili imkoniyatlaridan kelib chiqib fraymvork o'rganing.
                    </p>
                </div>

            </div>

            <div className="learn-prog-row row ">


                <div className="learn-prog-col col-lg-5 ml-lg-auto order-lg-1 order-md-1 col-md-8">
                    <h2 className="learn-prog-col-text-header">
                        04. Ishga ega bo'ling yoki katta loyihalar yarating.
                    </h2>
                    <p className="learn-prog-col-text">
                        Dasturlashda jamoa bo'lib ishlash juda muhim. Shuning uchun biror jamoaga borib qo'shiling yoki
                        o'z jamoangizni tuzing.
                    </p>
                </div>

                <div className="learn-prog-col col-lg-4 col-md-10 order-lg-2 order-md-2 mx-md-auto">
                    <img className="learn-prog-col-img w-100" src="images/task.svg"/>

                </div>
            </div>


        </section>    )
}

export default Section2;