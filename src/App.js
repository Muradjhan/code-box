import Navbar from "./components/Navbar";
import Section1 from "./components/Section";
import Footer from "./components/Footer";
import Section2 from "./components/Section-2";
import Section3 from "./components/Section-3";
import Section5 from "./components/Section-5";
import Section6 from "./components/Section-6";

function App() {
  return (
    <div className="App">
        <Navbar/>
        <Section1/>
        <Section2/>
        <Section3/>
        <Section5/>
        <Section6/>
        <Footer/>
    </div>
  );
}

export default App;
