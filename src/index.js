import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min';
import 'popper.js';
import 'bootstrap/dist/js/bootstrap.min';
import 'react-card-carousel'
// import 'bootstrap/dist/js/bootstrap.bundle.min.js';
// import $ from 'jquery';
// import Popper from 'popper.js';

// import 'bootstrap/js/jquery-3.3.1.slim.min';
// import 'bootstrap/js/popper.min.js';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
